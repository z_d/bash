#### **How to step by step setup wireless Access Point on Raspberry Pi.**

###### __My case. An access point on a Raspberry Pi with a DHCP_proxy.__

```
                                    ______                   ______
   ISP      ___________            |  PC  |                 |Laptop|
 connect   |           |<--------->|______|_        ``      |______|
---------->|  Router   |   LAN   |__________|      `````   /________\
   WAN     |Dhcp-Server|            __________    ``````     
           |___________|<--------->| AP Wi-Fi | `````````
                                   |Dhcp-Proxy|``````````` 2,4 GHz 
                                   |__________| ````````` 
                                                  ```````
                                                    ````
                                                     ``
```
- Hardware: Raspberry Pi 4

- Software: Ubuntu 20.04.2 LTS

###### Installing dnsmasq and hostapd:

- Dnsmasq it is light DHCP and DNS server. 

- Hostapd it is software for wireless Access Point.


> `#sudo apt-get update`

> `#sudo apt-get install dnsmasq hostapd`

###### Combining network interfaces into a bridge using netplan.


```
network:
    version: 2
    ethernets:
        eth0:
            dhcp4: false
        wlan0:
            dhcp4: false
    bridges:
        br0:
            dhcp4: true
            interfaces:
                - eth0
                - wlan0
            parameters:
                stp: true
                forward-delay: 4
```

> `#sudo netplan generate`

> `#sudo netplan apply`

###### Setup Dnsmasq:

> `#sudo cp /etc/dnsmasq.conf /etc/dnsmasq.conf.orig`

###### Create new config-file.

> `#sudo nano /etc/dnsmasq.conf`

```
port=0
interface=br0
log-dhcp
dhcp-authoritative
dhcp-range=192.168.1.0,proxy
dhcp-option=option:router,192.168.1.254
dhcp-option=6,8.8.8.8

```

###### Setup Hostapd.

> `#sudo cp /etc/hostapd/hostapd.conf /etc/hostapd/hostapd.conf.orig`

###### Create new config-file.

> `#sudo nano /etc/hostapd/hostapd.conf`

```
interface=wlan0
bridge=br0
driver=nl80211
ssid=Hot-Spot
country_code=RU
hw_mode=g
channel=13
ieee80211n=1
ieee80211d=1
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=Pa$$w0rd
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
wmm_enabled=1

```
###### Reboot services

> `#sudo systemctl restart hostapd dnsmasq`

> `#sudo systemctl status hostapd dnsmasq`

###### Happy end!!!
