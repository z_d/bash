#!/bin/bash
#Configure network
int = "enp0s3"
ip_addr=`ip addr show $int | awk '$1 == "inet" {gsub(/\/.*$/, "", $2); print $2}'`
network=`192.168.1.`
#Update and install other soft
yum update -y && yum install mc nano wget -y
#Install dnsmasq
yum install dnsmasq -y
/bin/mv /etc/dnsmasq.conf  /etc/dnsmasq.conf.backup
#Creating config dnsmasq
{
echo port=0
echo #Change lan-inerface if needed
echo interface=enp0s3
echo #bind-interfaces
echo domain=centos7.lan
echo # DHCP range-leases
echo dhcp-range=proxy,192.168.1.240,192.168.1.250,255.255.255.0,1h
echo # PXE
echo dhcp-boot=pxelinux.0,pxeserver,$ip_addr
echo # Gateway
echo dhcp-option=3,192.168.1.254
echo # DNS
echo dhcp-option=6,8.8.8.8, 8.8.4.4
echo server=8.8.4.4
echo # Broadcast Address
echo dhcp-option=28, 192.168.1.255
echo # NTP Server
echo dhcp-option=42,0.0.0.0

echo pxe-prompt="Press F8 for menu.", 60
echo pxe-service=x86PC, "Install CentOS 7 from PXE-Server", pxelinux
echo enable-tftp
echo tftp-root=/var/lib/tftpboot
} > /etc/dnsmasq.conf
#Install syslinux
yum install syslinux -y
#Install tftp-server
yum install tftp-server -y
#Creating dir and conf to pxe-server
cp -r /usr/share/syslinux/* /var/lib/tftpboot
mkdir /var/lib/tftpboot/pxelinux.cfg
{
echo default menu.c32
echo prompt 0
echo timeout 300
echo ONTIMEOUT local
echo 
echo menu title ########## PXE Boot Menu ##########
echo
echo label 1
echo 'menu label ^1) Install CentOS 7 x64 with Local Repo'
echo kernel centos7/vmlinuz
echo append initrd=centos7/initrd.img method=ftp://$ip_addr/pub devfs=nomount
echo 
echo label 2
echo 'menu label ^2) Install CentOS 7 x64 with http://mirror.centos.org Repo'
echo kernel centos7/vmlinuz
echo append initrd=centos7/initrd.img method=http://mirror.centos.org/centos/7/os/x86_64/ devfs=nomount ip=dhcp
echo
echo label 3
echo 'menu label ^3) Install CentOS 7 x64 with Local Repo using VNC'
echo kernel centos7/vmlinuz
echo append  initrd=centos7/initrd.img method=ftp://$ip_addr/pub devfs=nomount inst.vnc inst.vncpassword=password
echo
echo label 4
echo 'menu label ^4) Boot from local drive'
} > /var/lib/tftpboot/pxelinux.cfg/default
#Creating boot images
wget https://mirror.yandex.ru/centos/7.8.2003/isos/x86_64/CentOS-7-x86_64-DVD-2003.iso
mount -o loop $PWD/CentOS-7-x86_64-DVD-2003.iso  /mnt
mkdir /var/lib/tftpboot/centos7
cp /mnt/images/pxeboot/vmlinuz  /var/lib/tftpboot/centos7
cp /mnt/images/pxeboot/initrd.img  /var/lib/tftpboot/centos7
#Install vsftpd
yum install vsftpd -y
mkdir /var/ftp/pub
cp -r /mnt/*  /var/ftp/pub/
chmod -R 755 /var/ftp/pub
sed -i 's/anonymous_enable=NO$/anonymous_enable=YES/g' /etc/vsftpd/vsftpd.conf
#Start services
systemctl start dnsmasq
systemctl start vsftpd
systemctl enable dnsmasq
systemctl enable vsftpd
#Setup firewall
firewall-cmd --add-service=ftp --permanent
firewall-cmd --add-service=dns --permanent
firewall-cmd --add-service=dhcp --permanent  
firewall-cmd --add-port=69/udp --permanent  
firewall-cmd --add-port=4011/udp --permanent  
firewall-cmd --reload
#Umount
umount /mnt
exit 0
