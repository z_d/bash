#!/bin/bash
#Install Asterisk 16.13.0
#Update
yum -y update
#Disable SElinux
sed -i s/SELINUX=enforcing/SELINUX=disabled/g /etc/selinux/config
#Install other soft
yum install -y epel-release dmidecode gcc-c++ ncurses-devel libxml2-devel make wget openssl-devel newt-devel kernel-devel sqlite-devel libuuid-devel gtk2-devel jansson-devel binutils-devel libedit libedit-devel svn mc nano bzip2
#Creating directory "build" and download soft
mkdir ~/build && cd ~/build
wget https://www.pjsip.org/release/2.9/pjproject-2.9.tar.bz2
tar xvjf pjproject-2.9.tar.bz2
cd pjproject-2.9
./configure CFLAGS="-DNDEBUG -DPJ_HAS_IPV6=1" --prefix=/usr --libdir=/usr/lib64 --enable-shared --disable-video --disable-sound --disable-opencore-amr
make dep
make && sudo make install && sudo ldconfig
#Check out the library, if necessary  --> $ldconfig -p | grep pj
#Install Asterisk on CentOS 7
wget http://downloads.asterisk.org/pub/telephony/asterisk/asterisk-16-current.tar.gz
tar -zxvf asterisk-16-current.tar.gz
cd asterisk-16.13.0
sudo ./contrib/scripts/get_mp3_source.sh
sudo contrib/scripts/install_prereq install
./configure --libdir=/usr/lib64 --with-jansson-bundled
#Enable the music hold function --> make menuselect --> Add-ons --> format_mp3 --> Save & Exit
sudo make && sudo make install
sudo make samples
sudo make config
#Create user Asterisk and password
adduser asterisk -c "Asterisk User"
passwd asterisk 
usermod -aG wheel asterisk
#su asterisk
#To assign the rights to directories
sudo chown asterisk. /var/run/asterisk
sudo chown asterisk. -R /etc/asterisk
sudo chown asterisk. -R /var/{lib,log,spool}/asterisk
###Firewall rules
firewall-cmd --permanent --new-service=asterisk
firewall-cmd --permanent --service=asterisk --add-port=5060/tcp
firewall-cmd --permanent --service=asterisk --add-port=5060/udp
firewall-cmd --permanent --service=asterisk --add-port=5061/tcp
firewall-cmd --permanent --service=asterisk --add-port=5061/udp
firewall-cmd --permanent --service=asterisk --add-port=4569/udp
firewall-cmd --permanent --service=asterisk --add-port=5038/tcp
firewall-cmd --permanent --service=asterisk --add-port=10000-20000/udp
firewall-cmd --permanent --add-service=asterisk
firewall-cmd --reload
#Start the service --> sudo service asterisk restart
#Asterisk shell --> sudo asterisk -rvv