**Restart Quagga, Open Vpn and docker-nginx.**

#Add rule on cron.

```
sudo crontab -e

@reboot sleep 10 && cd /home/user/ && sh quaggaautorest.sh && sh 2peer.sh > /home/user/quagga.txt 2>&1

```
#Create bash-script quaggaautorest.sh

```
#!/bin/bash/

# Description:
# Auto restart after boot completed OS. Restarting Open Vpn and Quagga.
#   How to works:
#      1) Stop Open Vpn;
#      2) Stop Zebra, Ripd and Ospfd;
#      3) Start Open Vpn;
#      4) Start Zebra, Ripd and Ospfd.
#      5) Start Docker.

systemctl stop openvpn

sleep 5s

systemctl stop zebra ospfd ripd

sleep 5s

systemctl start openvpn

sleep 5s

systemctl start zebra ospfd ripd

docker start docker-nginx

echo "Restart services complited!  $(date)"

exit 0

```
