#!/bin/bash/

# Description:
# Auto restart after boot completed OS. Restarting Open Vpn and Quagga.
#   How to works:
#      1) Stop Open Vpn;
#      2) Stop Zebra and Ospfd;
#      3) Start Open Vpn;
#      4) Start Zebra and Ospfd.


/sbin/bash/ systemctl stop openvpn

/sbin/bash/ systemctl stop zebra ospfd

/sbin/bash/ systemctl start openvpn

/sbin/bash/ systemctl start zebra quagga
