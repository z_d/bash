Example simple iptables rules.

```
#!/bin/bash
#Firewall simple rules for CentOS 7
#sudo iptables -L -n -v
 
#Allow traffic to the loopback interface
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT 

#Allow outgoing traffic on the interface enp0s3
iptables -A OUTPUT -o enp0s3 -j ACCEPT

#Allow icmp
iptables -A INPUT -p icmp -j ACCEPT
iptables -A OUTPUT -p icmp -j ACCEPT

#Allow traffic for all established and related connections
iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT

#Allow ssh
iptables -A INPUT -p tcp -m conntrack --ctstate NEW -m tcp --dport 22 -j ACCEPT

#Deny other input traffic
iptables -P INPUT DROP
iptables -P OUTPUT DROP
iptables -P FORWARD DROP

#Network Address Translation
iptables -t nat -A POSTROUTING -o enp0s3 -j MASQUERADE

#Allow forward icmp-traffic
iptables -A FORWARD -p icmp -j ACCEPT

#Allow forward traffic for all established and related connections
iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT

#Allow forward traffic to ports 80 and 443
iptables -A FORWARD -p tcp -m conntrack --ctstate NEW -m multiport --dports 80,443 -j ACCEPT

#Allow forward traffic to port 53. For Domain Name System.
iptables -A FORWARD -p tcp -m conntrack --ctstate NEW -m tcp --dport 53 -j ACCEPT
iptables -A FORWARD -p udp -m conntrack --ctstate NEW -m udp --dport 53 -j ACCEPT

####https://www.youtube.com/watch?v=48r3dbgru1A
```
