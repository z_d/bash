

```
sudo iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 1443 -j DNAT --to-destination 10.7.0.2:1443  
```
```
sudo iptables -t nat -A POSTROUTING -o tun0 -p tcp --dport 1443 -d 10.7.0.2 -j SNAT --to-source 10.7.0.1
```
```
sudo iptables -A FORWARD -p tcp --dport 1443 -j ACCEPT
```
