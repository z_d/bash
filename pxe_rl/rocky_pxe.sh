#!/bin/bash
#Configure network
int="enp0s8"
ip_addr=`ip addr show $int | awk '$1 == "inet" {gsub(/\/.*$/, "", $2); print $2}'`
network="192.168.1."
#Update and install other soft
yum update -y && yum install mc nano wget python3 -y
pip3 install crypto
#Encrypt password
cryptpass=`python3 -c 'import crypt; print(crypt.crypt("Pa$$w0rd"))'`
#Install dnsmasq
yum install dnsmasq -y
/bin/mv /etc/dnsmasq.conf  /etc/dnsmasq.conf.backup
#Creating config dnsmasq
{
echo port=0
echo #Change lan-inerface if needed
echo interface=$int
echo #bind-interfaces
echo domain=rocky.lan
echo # DHCP range-leases
echo dhcp-range=proxy,$network\40,$network\50,255.255.255.0,1h
echo # PXE
echo dhcp-boot=pxelinux.0,pxeserver,$ip_addr
echo # Gateway
echo dhcp-option=3,$network\1
echo # DNS
echo dhcp-option=6,8.8.8.8, 8.8.4.4
echo server=8.8.4.4
echo # Broadcast Address
echo dhcp-option=28, $network\255
echo # NTP Server
echo dhcp-option=42,0.0.0.0

echo pxe-prompt="Press F8 for menu.", 10
echo pxe-service=x86PC, "Install Rocky Linux 8.4 from PXE-Server", pxelinux
echo enable-tftp
echo tftp-root=/var/lib/tftpboot
} > /etc/dnsmasq.conf
#Install syslinux
yum install syslinux -y
#Install tftp-server
yum install tftp-server -y
#Creating dir and conf to pxe-server
cp -r /usr/share/syslinux/* /var/lib/tftpboot
mkdir /var/lib/tftpboot/pxelinux.cfg
{
echo default menu.c32
echo prompt 0
echo timeout 300
echo
echo menu title ########## PXE Boot Menu ##########
echo
echo label 1
echo 'menu label ^1) Install Rocky Linux 8.4 x64 with Local Repo'
echo kernel rl/vmlinuz
echo append initrd=rl/initrd.img method=ftp://$ip_addr/pub devfs=nomount
echo
echo label 2
echo 'menu label ^2) Install Rocky Linux x64 with http://download.rockylinux.org Repo'
echo kernel rl/vmlinuz
echo append initrd=rl/initrd.img method=http://download.rockylinux.org/pub/rocky/8/BaseOS/x86_64/os devfs=nomount ip=dhcp
echo
echo label 3
echo 'menu label ^3) Install Rocky Linux x64 with Local Repo using VNC'
echo kernel rl/vmlinuz
echo append initrd=rl/initrd.img method=ftp://$ip_addr/pub devfs=nomount inst.vnc inst.vncpassword=password
echo
echo label 4
echo 'menu label ^4) Install Rocky Linux 8 Minimal Kickstart-installation'
echo kernel rl/vmlinuz
echo append initrd=rl/initrd.img inst.repo=ftp://$ip_addr/pub inst.ks=ftp://$ip_addr/pub/ksfile/rocky_kickstart.cfg
echo
echo label 5
echo 'menu label ^5) Boot from local drive'
echo menu default
echo localboot 0
} > /var/lib/tftpboot/pxelinux.cfg/default
#Creating boot images
wget https://download.rockylinux.org/pub/rocky/8/isos/x86_64/Rocky-8.4-x86_64-dvd1.iso
echo "/root/Rocky-8.4-x86_64-dvd1.iso /mnt iso9660 loop 0 0" >> /etc/fstab
mount -a
#mount -o loop $PWD/Rocky-8.4-x86_64-dvd1.iso  /mnt
mkdir /var/lib/tftpboot/rl
cp /mnt/images/pxeboot/vmlinuz  /var/lib/tftpboot/rl
cp /mnt/images/pxeboot/initrd.img  /var/lib/tftpboot/rl
#Install vsftpd
yum install vsftpd -y
mkdir /var/ftp/pub
mkdir /var/ftp/pub/ksfile
cp -r /mnt/*  /var/ftp/pub/
chmod -R 755 /var/ftp/pub
sed -i 's/anonymous_enable=NO$/anonymous_enable=YES/g' /etc/vsftpd/vsftpd.conf
#Creating kickstart-file
{
echo #version=RHEL8
echo # Use graphical install
echo graphical
echo # Use network installation
echo url --url="ftp://$ip_addr/pub/BaseOS"
echo repo --name="Minimal" --baseurl=ftp://$ip_addr/pub/BaseOS
echo
echo %packages
echo @^minimal-environment
echo @standard
echo kexec-tools
echo
echo %end
echo
echo # Keyboard layouts
echo keyboard --xlayouts='us'
echo # System language
echo lang en_US.UTF-8
echo
echo # Network information
echo network  --bootproto=dhcp --device=enp0s3 --ipv6=auto --activate
echo network  --hostname=Rocky_Linux
echo # Reboot after installation
echo reboot
echo
echo # Run the Setup Agent on first boot
echo firstboot --enable
echo
echo ignoredisk --only-use=sda
echo autopart
echo # Partition clearing information
echo clearpart --none --initlabel
echo
echo # System timezone
echo timezone Europe/Moscow --isUtc
echo
echo # Root password
echo rootpw --iscrypted $cryptpass
echo user --groups=wheel --name=user --password=$6$UEYuo0B.mtjZ0LYP$i20y18MHYgJSatnlL9/XoJYE1fwPF0B8kqR6VdEKowBX/E.7Tr/U27N7WuPuHmyoMpgRNoyWbL1m0llgWowUx1 --iscrypted --gecos="user"
echo
echo %addon com_redhat_kdump --enable --reserve-mb='auto'
echo
echo %end
echo
echo %anaconda
echo pwpolicy root --minlen=6 --minquality=1 --notstrict --nochanges --notempty
echo pwpolicy user --minlen=6 --minquality=1 --notstrict --nochanges --emptyok
echo pwpolicy luks --minlen=6 --minquality=1 --notstrict --nochanges --notempty
echo %end
} > /var/ftp/pub/ksfile/rocky_kickstart.cfg
#Start services
systemctl start dnsmasq
systemctl start vsftpd
systemctl enable dnsmasq
systemctl enable vsftpd
#Setup firewall
firewall-cmd --add-service=ftp --permanent
firewall-cmd --add-service=dns --permanent
firewall-cmd --add-service=dhcp --permanent
firewall-cmd --add-port=69/udp --permanent
firewall-cmd --add-port=4011/udp --permanent
firewall-cmd --reload
#Umount
umount /mnt
exit 0
