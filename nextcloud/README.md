### Install nextcloud with docker-container.

https://hub.docker.com/_/nextcloud/

Download and install nextcloud-image. 

> docker pull nextcloud

Install docker-container and connect your folder to the docker-container.

> docker run -d -p 9090:80 -v /you/share/:/var/www/html/data nextcloud

Port forwarding to iptables.

> sudo iptables -t nat -A PREROUTING -p tcp --dport 9090 -j DNAT --to-destination ip-address:9090

Download and install Windows-app Nextcloud

https://github.com/nextcloud/desktop/releases/download/v3.3.4/Nextcloud-3.3.4-x64.msi
